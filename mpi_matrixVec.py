import sys
import math
import time
import numpy as np
from scipy.sparse import csr_matrix,csc_matrix

from mpi4py import MPI 
comm = MPI.COMM_WORLD


#step 1 : read in csv file and data distribution


my_rank = comm.Get_rank()
p = comm.Get_size()


val = None
row_ptr = None
col_ind = None
vec_data = None
n = None
vec_len = None


vSize = [None]*p
vDisp = [None]*p
mSize = [None]*p
mDisp = [None]*p

total_result = None
if my_rank == 0:
    ##
    #step 1: read in and distribute data
    matrix_name = sys.argv[1].split(".")[0]

    def readData():
        file_handle = open(matrix_name+str(".txt"), 'r')
        lines_list = file_handle.readlines()
        
        # assume the file contains 3 lines in crs format
        val = [float(val) for val in lines_list[0].split()]
        row_ptr = [int(val) for val in lines_list[1].split()]
        col_ind = [int(val) for val in lines_list[2].split()]
        nnz = len(val)
        n = len(row_ptr) - 1
        
        #print(col_ind)
        file_handle.close()

        file_handle = open(matrix_name+str("_vec.txt"), 'r')
        lines_list = file_handle.readlines()
        vec_data = [float(val) for val in lines_list[0].split()]
        vec_len = len(vec_data)
        vec_data = np.array(vec_data)
        #print(vec_data)
        file_handle.close()

        return [val,row_ptr,col_ind,vec_data,nnz,n,vec_len]
    [val,row_ptr,col_ind,vec_data,nnz,n,vec_len] = readData()

    def getVecSizeDisp():
        vsize = math.ceil(float(vec_len)/p)#partial vector size for each
        vsize2 = vec_len - (p-1)*vsize#vector size for last process
        if vsize2 < 0:#error case
            print('Invalid number of processors')    

        if my_rank == p - 1:
            vsize = vsize2

        #vector size and displacements for each processor
        vSize = [vsize]*p
        vSize[-1] = vsize2
        vSize = tuple(vSize)
        
        vDisp = [i*vsize for i in range(p)]
        #vDisp[-1] = (p-1)*vSize
        vDisp = tuple(vDisp)

        return [vSize,vDisp]
    [vSize,vDisp]=getVecSizeDisp()

    size = vSize[0]#"standard" length
    #matrix size and displacements for each processor
    #print(vSize)
    def getMatSizeDisp(row_ptr,vSize):
        mSize = [row_ptr[i*size+vSize[i]] - row_ptr[i*size] for i in range(p)]
        mSize = tuple(mSize)
        mDisp = [row_ptr[i*size] for i in range(p)]#distance from the 1st element
        mDisp = tuple(mDisp)
        return [mSize,mDisp]
    [mSize,mDisp] = getMatSizeDisp(row_ptr,vSize)

    total_result = np.zeros(n)
t0 = MPI.Wtime()

def bcastSizeDisp(vSize,vDisp,mSize,mDisp):
    vSize = comm.bcast(vSize)#to broadcast tuple,lowercase is enough
    #print(vSize)

    vDisp = comm.bcast(vDisp)
    #print(vDisp)

    mSize = comm.bcast(mSize)
    #print(mSize)

    mDisp = comm.bcast(mDisp)
    #print(mDisp)
    return [vSize,vDisp,mSize,mDisp]
[vSize,vDisp,mSize,mDisp] = bcastSizeDisp(vSize,vDisp,mSize,mDisp)

#use scatterv to distribute vector
vec_local = np.zeros(vSize[my_rank])
comm.Scatterv([vec_data,vSize,vDisp,MPI.DOUBLE],vec_local)
#print("rank" + str(my_rank)+ "vec" + str(vec_local))

val = np.array(val)
row_ptr = np.array(row_ptr,dtype = 'float64')
col_ind = np.array(col_ind,dtype='float64')#somehow default int64 and mpi.long wont work in Scatterv
    
#scatter and rewrite row_ptr
row_ptr_local = np.zeros(vSize[my_rank])
comm.Scatterv([row_ptr,vSize,vDisp,MPI.DOUBLE],row_ptr_local)
#rewrite
row_ptr_local = row_ptr_local - row_ptr_local[0]
row_ptr_local = np.append(row_ptr_local,mSize[my_rank])
#print("rank"+str(my_rank)+"row_ptr"+str(row_ptr_local))

#scatter val 
val_local = np.zeros(mSize[my_rank])
comm.Scatterv([val,mSize,mDisp,MPI.DOUBLE],val_local)
#print("rank" + str(my_rank) + "mat" + str(val_local))

#scatter col_ind
col_ind_local = np.zeros(mSize[my_rank])
comm.Scatterv([col_ind,mSize,mDisp,MPI.DOUBLE],col_ind_local)
#print("rank" + str(my_rank) + "col_ind" + str(col_ind_local))



##
#step2: analyze elements
size = vSize[0]#"standard" size
localVecLen = vSize[my_rank]
localIndStart = my_rank*size
localIndEnd = localIndStart + vSize[my_rank] - 1
IndByProc = [[] for _ in range(p)]
localDependency = [[] for _ in range(p)]#with p cells and each cell contains the needed index from the corresponding core

def divideSubBlock(mSize,col_ind_local,localDependency,IndByProc):
#Divide the local matrix into "sub-block" by processors including diagonal one
    for i in range(mSize[my_rank]):
        #including diagonal block
        #if col_ind_local[i] < localIndStart or col_ind_local[i] > localIndEnd:
        proc = math.floor(col_ind_local[i]/size)
    
        localDependency[proc].append(col_ind_local[i])
        IndByProc[proc].append(i)

    #sort and remove duplicates from localDependency
    for i in range(p):
        localDependency[i] = sorted(list(set(localDependency[i])))
    
    return [localDependency,IndByProc]
[localDependency,IndByProc] = divideSubBlock(mSize,col_ind_local,localDependency,IndByProc)

#create row pointer array for each "sub_block" including diagonal block
RowPtrByProc = [[0]*(localVecLen+1) for _ in range(p)]
def createRowPtr(RowPtrByProc,IndByProc,row_ptr_local):
    for proc in range(p):
        #RowPtrByProc[proc][0] = 0
        row = 0
        for j in range(len(IndByProc[proc])):
            #what if there is empty row??(highly likely)--Use while
            while IndByProc[proc][j] >= row_ptr_local[row+1]: # row can be n-1 at best(and should be n-1)
                row = row + 1
                RowPtrByProc[proc][row] = j
            #what if last few rows are empty
            else:
                if j == len(IndByProc[proc])-1 and row < localVecLen - 1:#last few rows are empty
                    for k in range(row+1,localVecLen):
                        RowPtrByProc[proc][k] = j + 1

        RowPtrByProc[proc][-1] = len(IndByProc[proc])#the last element of row_ptr should be nnz(0 based)

        #return RowPtrByProc why???
createRowPtr(RowPtrByProc,IndByProc,row_ptr_local)


#dont have to send out diagonal dependencies
#localDependency[my_rank] = []

adjMat = comm.allgather(localDependency) 
comm.barrier()
t1 = MPI.Wtime()
#print("my rank"+str(my_rank))
#print("vSize"+str(vSize))
#print(len(adjMat[my_rank][my_rank]))
#print(adjMat)
#print(IndByProc)

#sending remote vector
sendReq = []
def createSendQueue(sendReq,adjMat):
    for i in range(p):
        trunk_len = len(adjMat[i][my_rank])
        if i != my_rank and trunk_len is not 0:
            trunk = np.zeros(trunk_len)
            for j in range(trunk_len):
                local_index_tmp = int(adjMat[i][my_rank][j] - size*my_rank )#except here and dense multiplication, vec index refers to global index
                trunk[j] = vec_local[local_index_tmp]

            req = comm.Isend([trunk,MPI.DOUBLE],dest = i, tag = my_rank*p+i)#tag: root*p + dest
            sendReq.append(req)
    return sendReq
sendReq = createSendQueue(sendReq,adjMat)

#receiving remote vector
recReq = []
#receiving buffer
data = [np.zeros(len(adjMat[my_rank][i])) for i in range(p)]
def createRecQueue(adjMat,recReq,data):
    recCount = 0
    for i in range(p):
        trunk_len = len(adjMat[my_rank][i])
        
        if i != my_rank and trunk_len is not 0:

            req = comm.Irecv([data[i],MPI.DOUBLE],source = i,tag = i*p + my_rank)
            recReq.append(req)
            recCount = recCount + 1
        
    return [recCount,recReq,data]
[recCount,recReq,data] = createRecQueue(adjMat,recReq,data)

#compute the diagonal part first: sparse matrix * dense vector
#same procedure with remote part(can make a function later)



val_tmp = val_local[IndByProc[my_rank]]
#rewrite local col indices
ind_tmp = col_ind_local[IndByProc[my_rank]] - vDisp[my_rank]

row_tmp = RowPtrByProc[my_rank]
vecInd_tmp = adjMat[my_rank][my_rank]

def compDiag(val_tmp,ind_tmp,row_tmp,vecInd_tmp,vec_local):
    
    if len(val_tmp) == 0 or len(ind_tmp)==0 or len(row_tmp)==0: 
        
        result = np.zeros(vSize[my_rank])
    else:
        diag_crs = csr_matrix((val_tmp,ind_tmp,row_tmp),shape=(localVecLen,localVecLen))
        
        #local_index_tmp = [int(vecInd_tmp[i]) - my_rank*size for i in range(len(vecInd_tmp))]
        #print('rank{}'.format(my_rank))
        #print('diag_crs dim{}'.format(diag_crs.get_shape()))
        #print('vec_local size{}'.format(len(vec_local)))
        result = diag_crs.dot(vec_local)

    return result
result = compDiag(val_tmp,ind_tmp,row_tmp,vecInd_tmp,vec_local)

def fillVec(length,ind,data):
    #fill the sparse vec into dense with 0
    newVec = np.zeros(length)
    if len(ind)!= len(data): print("error : length of indices and data are not equal")
    for i in range(len(data)):
        newVec[int(ind[i])] = data[i]
    
    return newVec
#compute once a trunk has been received
def compRemote(val_local,col_ind_local,RowPtrByProc,adjMat,recReq,data,result):
    for i in range(recCount):
        status = MPI.Status()
        MPI.Request.waitany(recReq,status)#interleaf computation and communication(reindex the col_ind_local?
        source = status.Get_source()

        val_tmp = val_local[IndByProc[source]]
        
        ind_tmp = col_ind_local[IndByProc[source]] - vDisp[source]
        
        row_tmp = RowPtrByProc[source]
        
        remote_crs = csr_matrix((val_tmp,ind_tmp,row_tmp),shape=(localVecLen,vSize[source]))
        
        vecInd_tmp = [adjMat[my_rank][source][i] - vDisp[source] for i in range(len(adjMat[my_rank][source]))]
        length = vSize[source]
        #print('rank{}'.format(my_rank))
       
        remoteVec = fillVec(length,vecInd_tmp,data[source])
        
        remote_result = remote_crs.dot(remoteVec)
        
        for i in range(len(result)):
            result[i] = result[i] + remote_result[i]

        #return result #why?
compRemote(val_local,col_ind_local,RowPtrByProc,adjMat,recReq,data,result)
MPI.Request.waitall(sendReq)

#print("rank"+str(my_rank)+"result"+str(result))


comm.Gatherv(result,[total_result,vSize,vDisp,MPI.DOUBLE])

t2 = MPI.Wtime()
if my_rank == 0: 
    #print("total_result"+str(total_result))
    #file_handle = open(matrix_name+str("_result.txt"), 'r')
    #lines_list = file_handle.readlines()
    #result_data = [float(val) for val in lines_list[0].split()]
    #result_data = np.array(result_data)
    #print(vec_data)

    total_matrix = csr_matrix((val,col_ind,row_ptr))
    result_data = total_matrix.dot(vec_data)
    

    #distribution time
    t_dist = t1 -t0
    #comptation time
    t_comp = t2 -t1

    err = total_result-result_data
    err_max = np.linalg.norm(err,np.inf)
    err_2norm = np.linalg.norm(err)

    file_handle = open(matrix_name+str("_log.txt"),'a+')
    print('core counts: {}'.format(p),file = file_handle)
    print('distibuted in {:.3f} sec'.format(t_dist),file = file_handle)
    print('computed in {:.3f} sec'.format(t_comp),file = file_handle)
    print('error max norm is {}'.format(err_max),file=file_handle)
    print('error 2 norm is {}'.format(err_2norm),file=file_handle)
    file_handle.close()
